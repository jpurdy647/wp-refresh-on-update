<?php
/*
Plugin Name: WP Refresh on Update
Description: A simple plugin that refreshes a single post/page when it is updated on the server.
Author: Joshua Purdy
Version: 0.1
*/
defined( 'ABSPATH' ) or die();

function add_wp_refresh_on_update_metabox() {
    global $post;

	/*$post_types = array_merge(
						get_post_types( $args, 'names' ),
						['post', 'page']
					);
*/
    add_meta_box(
        "wp-refresh-on-update-metabox",
        __("Refresh on update"),
		'wp_refresh_on_update_metabox_callable',
        'page',
        'side',
		'low');
}

add_action("add_meta_boxes", "add_wp_refresh_on_update_metabox");

function wp_refresh_on_update_metabox_callable() {
    global $post;
	$checked = get_post_meta($post->ID,'_refresh_on_update_meta_key',true) == 'on';
	?>
	
	<input type="checkbox" name="refresh_on_update" <?php echo $checked ? 'checked' : '';?>> 
	<label for="refresh_on_update">Refresh Automatically</label>
	
	<?php
}

function wp_refresh_on_update_save_postdata($post_id)
{
	update_post_meta(
		$post_id,
		'_refresh_on_update_meta_key',
		array_key_exists('refresh_on_update', $_POST) ? 'on' : 'off'
	);
}
add_action('save_post', 'wp_refresh_on_update_save_postdata');


add_action( 'wp_footer', 'wp_refresh_on_update_check_page_updated' );
function wp_refresh_on_update_check_page_updated() { 
	$wp_refresh_on_update_check_page_updated_delay_millis = 10000;
	global $post;
	
	if (get_post_meta($post->ID,'_refresh_on_update_meta_key',true) == 'on') {
	
		?>
		<script type="text/javascript" >
		
			function jpComparePageUpdateTime(){
				var data = {
					'action': 'wp_refresh_on_update_compare_update_time',
					'client_page_id': <?php echo $post->ID; ?>,
					'client_page_updated_time': '<?php echo get_post_modified_time('Y-m-d H:i:s',false,$post->ID); ?>'
				};

				jQuery.post(ajaxurl, data, function(response) {
					if (response == 'false')
						location.reload();
				});
				
				setTimeout(jpComparePageUpdateTime, <?php echo $wp_refresh_on_update_check_page_updated_delay_millis; ?>);
			}
		
			jQuery(document).ready(function($) {
				setTimeout(jpComparePageUpdateTime, <?php echo $wp_refresh_on_update_check_page_updated_delay_millis; ?>);
			});
		</script> <?php
	}
}

add_action( 'wp_ajax_wp_refresh_on_update_compare_update_time', 'wp_refresh_on_update_server_compare_page_update_time' );
function wp_refresh_on_update_server_compare_page_update_time() {
	global $wpdb; // this is how you get access to the database

	$client_page_id = esc_sql( $_POST['client_page_id']) ;
	$client_page_updated_time =esc_sql( $_POST['client_page_updated_time']) ;

	$server_page_updated_time = $wpdb->get_var("SELECT post_modified FROM $wpdb->posts WHERE ID = $client_page_id" );
	
	echo $client_page_updated_time == $server_page_updated_time ? 'true' : 'false';
	
	wp_die();
}

